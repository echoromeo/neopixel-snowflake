/*
 * main.c
 *
 * Created: 31.08.2018 19:07:35
 * Author : Terje
 */

/*
 * Include files
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <util/atomic.h>
#include "neopixel.h"


/*
 * Enums and defines
 */
enum modes {
	HUE_CIRCLE,
	CHRISTMAS_NO1,
	CHRISTMAS_NO2,
	NUM_MODES,
} mode = HUE_CIRCLE;

#define SLEEP_FREQUENCY 10					// 10 Hz
#define LIGHTS_OFF (4*SLEEP_FREQUENCY)		// 4 sec
#define LIGHTS_UP ((SLEEP_FREQUENCY*3)/4)	// 3/4 sec
#define CHANGE_MODE (1)						// Debounce

/*
 * Global variables
 */
// The array containing the NeoPixel configuration
color_t neopixels[RGB_LEDSTRING_LENGTH] = {0};
static volatile uint8_t brightness = 0x30;
static volatile uint8_t button_pressed = 0;

/*
 * Functions
 */
void handle_button(void);
void full_hue_circle(void);
void christmas_no1(void);
void christmas_no2(void);
/*
 * main()
 */

int main(void)
{
	// Configure clock
#if (F_CPU == 20000000ul) || (F_CPU == 16000000ul)
	_PROTECTED_WRITE(CLKCTRL.MCLKCTRLB, 0);
#elif (F_CPU == 10000000ul) || (F_CPU == 8000000ul)
	_PROTECTED_WRITE(CLKCTRL.MCLKCTRLB, CLKCTRL_PDIV_2X_gc | CLKCTRL_PEN_bm);
#endif

	// Init LEDs to black
	LED_Init();
	LED_Fill_LED_RGB_String(BLACK, RGB_LEDSTRING_LENGTH);

	// Configure RTC to wake up device after sleep
	RTC.INTCTRL = RTC_OVF_bm;
	RTC.PER = (1024 / SLEEP_FREQUENCY); // Change this to control update interval
	RTC.CLKSEL = RTC_CLKSEL_INT1K_gc;
	RTC.CTRLA = RTC_RTCEN_bm | RTC_PRESCALER_DIV1_gc | RTC_RUNSTDBY_bm;
	set_sleep_mode(SLEEP_MODE_STANDBY);
	sei();
	sleep_mode();
	
	// Configure pullup for active low switch
	PORTC.PIN0CTRL = PORT_INVEN_bm | PORT_PULLUPEN_bm | PORT_ISC_RISING_gc;

	while(1) {

		handle_button();

		switch (mode)
		{
			default:
			case HUE_CIRCLE:
				full_hue_circle();
				break;

			case CHRISTMAS_NO1:
				christmas_no1();
				break;

			case CHRISTMAS_NO2:
				christmas_no2();
				break;
		}

		// Update LEDs, use atomic block to prevent interrupts during update
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
			LED_Write_LED_RGB_String((uint24_t *)neopixels, RGB_LEDSTRING_LENGTH);
		}

		// Go to sleep, wake up on RTC ISR
		sleep_mode();
	}
}

void handle_button(void) {
	if (PORTC.IN & PIN0_bm)
	{
		button_pressed++;
		if (button_pressed > LIGHTS_OFF) // off
		{
			brightness = 0;
		}	
	} 
	else 
	{
		if (button_pressed > LIGHTS_OFF) // reset mode
		{
			mode = HUE_CIRCLE;
		} 
		else if (button_pressed >LIGHTS_UP) // long press
		{
			brightness += 0x20;
		} 
		else if (button_pressed > CHANGE_MODE) // short press
		{
			if (!brightness)
			{
				brightness = 0x10;
			} 
			else 
			{			
				mode = (mode+1) % NUM_MODES;
			}
		}
		button_pressed = 0;
	}
}

ISR(RTC_CNT_vect)   
{
	// Nothing to do here, just for wakeup
	RTC.INTFLAGS = RTC_OVF_bm;
}

ISR(PORTC_PORT_vect) 
{
	button_pressed++; // In case of interrupt during LED update
	PORTC.INTFLAGS = PIN0_bm;
}

void full_hue_circle(void) {
	// Variable to keep track of ring "rotation"
	static uint8_t move = 0;

	// Rotate LED ring with all hues up to 1535
	for(uint8_t i = 0; i < RGB_LEDSTRING_LENGTH; i++) {
		hsb_t new_setting = {
			.h = ((uint32_t) 1535ul * (i + 1)) / RGB_LEDSTRING_LENGTH,
			.s = 0xff, // Full saturation
			.b = brightness
		};
		color_t converted = hsb2rgb(new_setting);
		if (converted.r == 1)
		{
			converted.r = 0;
		}
		if (converted.g == 1)
		{
			converted.g = 0;
		}
		if (converted.b == 1)
		{
			converted.b = 0;
		}
		neopixels[(i + move) % RGB_LEDSTRING_LENGTH] = converted;
	}

	move = (move + 1) % RGB_LEDSTRING_LENGTH;	
}

void christmas_no1(void) {
	// Variable to keep track of ring "rotation"
	static uint8_t step = 0;
	uint8_t midstep = step;
	
	neopixels[midstep] = (color_t) {.g = brightness};
	midstep = (midstep + 1) % RGB_LEDSTRING_LENGTH;
	neopixels[midstep].g = (brightness*3)/9;
	neopixels[midstep].r = (brightness*5)/9;
	midstep = (midstep + 1) % RGB_LEDSTRING_LENGTH;
	neopixels[midstep].g = (brightness*3)/9;
	neopixels[midstep].r = (brightness*5)/9;
	midstep = (midstep + 1) % RGB_LEDSTRING_LENGTH;
	neopixels[midstep].g = (brightness*2)/9;
	neopixels[midstep].r = (brightness*6)/9;
	midstep = (midstep + 1) % RGB_LEDSTRING_LENGTH;
	neopixels[midstep].g = (brightness*1)/9;
	neopixels[midstep].r = (brightness*7)/9;
	midstep = (midstep + 1) % RGB_LEDSTRING_LENGTH;
	neopixels[midstep] = (color_t) {.r = brightness};
	midstep = (midstep + 1) % RGB_LEDSTRING_LENGTH;
	neopixels[midstep] = (color_t) {.r = brightness};
	midstep = (midstep + 1) % RGB_LEDSTRING_LENGTH;
	neopixels[midstep] = (color_t) {.r = brightness};
	
	step = (step + 1) % RGB_LEDSTRING_LENGTH;
}

void christmas_no2 (void) {
	// Variable to keep track of ring "rotation"
	static uint8_t greenstep = 0;
	static uint8_t redstep = RGB_LEDSTRING_LENGTH/2-1;

	neopixels[redstep] = (color_t) {.r = brightness};
	redstep = (redstep + 1) % RGB_LEDSTRING_LENGTH;
	neopixels[redstep] = (color_t) {.r = brightness/2};

	neopixels[greenstep] = (color_t) {.g = brightness};
	greenstep = (greenstep + 1) % RGB_LEDSTRING_LENGTH;
	neopixels[greenstep] = (color_t) {.g = brightness/2};
}
